//////////////////////////////////////////////////////
//  droneArucoEyeROSModuleNode.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Jan 15, 2014
//      Author: joselusl
//
//////////////////////////////////////////////////////



//I/O stream
//std::cout
#include <iostream>


//Opencv
#include <opencv2/opencv.hpp>

//ROS
#include "ros/ros.h"

//Aruco Eye
#include "droneArucoEyeROSModule.h"


#include "nodes_definition.h"


using namespace std;






int main(int argc,char **argv)
{
	//Ros Init
    ros::init(argc, argv, MODULE_NAME_ARUCO_EYE);
  	ros::NodeHandle n;
  	
    cout<<"[ROSNODE] Starting "<<ros::this_node::getName()<<endl;
  	
    DroneArucoEyeROSModule MyDroneArucoEye;
    MyDroneArucoEye.open(n);


  	
	try
	{
        ros::spin();
    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }

    return 1;
}
